///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab02c - Cat `n Mouse - EE 205 - Spr 2022
///
/// This is a classic "I'm thinking of a number" guessing game.  The mouse
/// will think of a number... and the cat will keep trying to guess it.
///
/// @file    catNmouse.c
/// @version 1.0 - Initial version
///
/// Compile: $ gcc -o catNmouse catNmouse.c
///
/// Usage:  catNmouse [n]
///   n:  The maximum number used in the guessing game
///
/// Exit Status:
///   1:  The command line parameter was not a valid value
///   0:  The cat finally guessed correctly
///
/// Example:
///   $ ./catNmouse 2000
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1385
///   No cat... the number I’m thinking of is smaller than 1385
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1382
///   No cat... the number I’m thinking of is larger than 1382
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1384
///   You got me.
///   |\---/|
///   | o_o |
///    \_^_/
///   
/// @author  Mina Girgis @mgirgis@hawaii.edu>
/// @date    01/27/2022
///////////////////////////////////////////////////////////////////////////////


#include <stdio.h>
/*
int main( int argc, char* argv[] ) {
   printf( "Cat `n Mouse\n" );
   printf( "The number of arguments is: %d\n", argc );

   // This is an example of getting a number from a user
   // Note:  Don't enter unexpected values like 'blob' or 1.5.  
   int aGuess;
   printf( "Enter a number: " );
   scanf( "%d", &aGuess );
   return 1;  // This is an example of how to return a 1
}*/
int main( int argc, char* argv[] ){

	         int aGuess, number;
		if(argc>1 ){
			number = atoi(argv[1]);
			if (number == 0){
				printf("input should be > 0\n");
				return 1;
			}
		}

		else{
			number = 2048;
		}
		 // printf("OK cat, I'm thinking of a number from 1 to 2000.  Make a guess: ");
		srand(time(0));
		//rand() % number;
		number = rand() % number+1; 
	// +1 makes the rand number start at 1 not 0.
		  do {
			scanf( "%d", &aGuess);
	    
			if(aGuess > number ) {
		   	
				printf(" No cat... the number I’m thinking of is smaller than %d \n", aGuess);
				
													                    }

	                if(aGuess < number) {
	     
				printf("No cat... the number I’m thinking of is larger than %d\n" , aGuess);

				
			}

	                if (aGuess == number) {
		   
		    		  printf("you got me\n");


				  
				   
				   
				      printf("      /\_/\  ( \n");
				      printf("     ( ^.^ ) ) \n");
				      printf("       \"/  (  \n");
				      printf("      ( | | ) \n" );
				      printf("     (__d b__)\n");
				    
				   
				   


			  break;
			                  }

			 if (aGuess<1){
				 printf("invalid input\n");
				    
	    		 }


			         } while (aGuess != number);


		          return 0;
}
